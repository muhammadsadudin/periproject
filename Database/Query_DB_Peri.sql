create table Design_Difficulty
(
pk_id_dd int identity primary key,
difficulty varchar(50),
design_price float,
)

create table product
(
pk_id_product int identity primary key,
product_name varchar(50),
product_price float
)

create table spending_table
(
pk_id_spending int identity primary key,
spending_name varchar(250),
spending_cash float,
date datetime
)

create table customer(
pk_id_customer int identity primary key,
name varchar(50),
phone varchar(50),
email varchar(50)
)

create table order_table(
pk_id_order int identity primary key,
fk_customer_id int,
details_order varchar(600),
lenght_meter float,
width_meter float,
fk_design_id int,
fk_product_id int,
amount float,
price float,
payment float,
date datetime,
payment_status varchar(20),
order_status varchar(20),
constraint fk_design foreign key (fk_design_id)
references Design_Difficulty(pk_id_dd),
constraint fk_product foreign key (fk_product_id)
references product (pk_id_product),
constraint fk_customer foreign key (fk_customer_id)
references customer(pk_id_customer)
)

