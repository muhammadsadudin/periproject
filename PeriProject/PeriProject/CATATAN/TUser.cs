﻿
//CATATAN
//Untuk isi get and Set pada model di TUser nya bisa ditambahkan seperti ini

namespace PeriProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class TUser
    {
        public int UserId { get; set; }
      
        public string UserName { get; set; }
        
        public string Password { get; set; }
        public string LoginErrorMessage { get; set; }
        public Nullable<int> Fk_AksesId { get; set; }

        public virtual UserAks UserAks { get; set; }

    }
}