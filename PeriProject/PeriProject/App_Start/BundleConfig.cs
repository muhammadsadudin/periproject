﻿using System.Web;
using System.Web.Optimization;

namespace PeriProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //Untuk Script js template Login
            bundles.Add(new ScriptBundle("~/Scripts_Login/js").Include(
                      "~/Scripts_Login/jquery-3.2.1.min.js",
                      "~/ Scripts_Login / animsition.min.js",
                      "~/Scripts_Login/popper.js",
                      "~/Scripts_Login/bootstrap.min.js",
                      "~/Scripts_Login/select2.min.js",
                      "~/Scripts_Login/moment.min.js",
                      "~/Scripts_Login/daterangepicker.js",
                      "~/Scripts_Login/countdowntime.js",
                      "~/Scripts_Login/main.js"
                      ));

            //Untuk Content css style template Login
            bundles.Add(new StyleBundle("~/Content_Login/css").Include(
                      "~/Content_Login//bootstrap.min.css",
                      "~/Content_Login/bootstrap.min.css",
                      "~/Content_Login/font-awesome.min.css",
                      "~/Content_Login/material-design-iconic-font.min.css",
                      "~/Content_Login/animate.css",
                      "~/Content_Login/hamburgers.min.css",
                      "~/Content_Login/animsition.min.css",
                      "~/Content_Login/select2.min.css",
                      "~/Content_Login/daterangepicker.css",
                      "~/Content_Login/util.css",
                      "~/Content_Login/main.css"
                ));

            //Untuk Content css style templae HomeCustomer
            bundles.Add(new StyleBundle("~/Content_HomeCustomer/css").Include(
                      "~/Content_HomeCustomer/open-iconic-bootstrap.min.css",
                      "~/Content_HomeCustomer/animate.css",
                      "~/Content_HomeCustomer/owl.carousel.min.css",
                      "~/Content_HomeCustomer/owl.theme.default.min.css",
                      "~/Content_HomeCustomer/magnific-popup.css",
                      "~/Content_HomeCustomer/aos.css",
                      "~/Content_HomeCustomer/ionicons.min.css",
                      "~/Content_HomeCustomer/bootstrap-datepicker.css",
                      "~/Content_HomeCustomer/jquery.timepicker.css",
                      "~/Content_HomeCustomer/flaticon.css",
                      "~/Content_HomeCustomer/icomoon.css",
                      "~/Content_HomeCustomer/style.css"
                ));
        }
    }
}
