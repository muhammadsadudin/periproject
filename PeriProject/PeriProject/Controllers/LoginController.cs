﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PeriProject.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult FormLogin()
        {
            return View();
        }

        public ActionResult Autherize(PeriProject.Models.TUser umodel)
        {
            using (Db_PeriEntities db = new Db_PeriEntitie())
            {
                var userDetails = db.TUsers.Where(x => x.UserName == umodel.UserName && x.Password == umodel.Password).FirstOrDefault();
                if (userDetails == null)
                {
                    umodel.LoginErrorMessage = "Username or Password Wrong !";
                    return View("FormLogin", umodel);
                }
                else
                {
                    Session["userID"] = userDetails.UserId;
                    Session["userName"] = userDetails.UserName;

                    if (userDetails.Fk_AksesId == 1)
                    {
                        return RedirectToAction("About", "Home"); // Masuk Ke View /Home/About ini diganti dengan view employee
                    }
                    else if (userDetails.Fk_AksesId == 2)
                    {
                        return RedirectToAction("Index", "Home"); // Masuk ke View /Home/Index halaman view owner
                    }
                    else
                    {
                        return RedirectToAction("FormLogin", "Login");
                    }
                }
            }
        }
        public ActionResult LogOut()
        {
            int userId = (int)Session["userID"];
            Session.Abandon();
            return RedirectToAction("FormLogin", "Login");
        }
    }
}