﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PeriProject.Models;

namespace PeriProject.Controllers.Owner
{
    public class spending_tableController : Controller
    {
        private Db_PeriEntities db = new Db_PeriEntities();

        // GET: spending_table
        public ActionResult Index()
        {
            return View(db.spending_table.ToList());
        }

        // GET: spending_table/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            spending_table spending_table = db.spending_table.Find(id);
            if (spending_table == null)
            {
                return HttpNotFound();
            }
            return View(spending_table);
        }

        // GET: spending_table/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: spending_table/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_spending,spending_name,spending_cash,date")] spending_table spending_table)
        {
            if (ModelState.IsValid)
            {
                db.spending_table.Add(spending_table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(spending_table);
        }

        // GET: spending_table/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            spending_table spending_table = db.spending_table.Find(id);
            if (spending_table == null)
            {
                return HttpNotFound();
            }
            return View(spending_table);
        }

        // POST: spending_table/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_spending,spending_name,spending_cash,date")] spending_table spending_table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spending_table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spending_table);
        }

        // GET: spending_table/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            spending_table spending_table = db.spending_table.Find(id);
            if (spending_table == null)
            {
                return HttpNotFound();
            }
            return View(spending_table);
        }

        // POST: spending_table/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            spending_table spending_table = db.spending_table.Find(id);
            db.spending_table.Remove(spending_table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
