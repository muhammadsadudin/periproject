﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PeriProject.Models;

namespace PeriProject.Controllers.Owner
{
    public class Design_DifficultyController : Controller
    {
        private Db_PeriEntities db = new Db_PeriEntities();

        // GET: Design_Difficulty
        public ActionResult Index()
        {
            return View(db.Design_Difficulty.ToList());
        }

        // GET: Design_Difficulty/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Design_Difficulty design_Difficulty = db.Design_Difficulty.Find(id);
            if (design_Difficulty == null)
            {
                return HttpNotFound();
            }
            return View(design_Difficulty);
        }

        // GET: Design_Difficulty/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Design_Difficulty/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pk_id_dd,difficulty,design_price")] Design_Difficulty design_Difficulty)
        {
            if (ModelState.IsValid)
            {
                db.Design_Difficulty.Add(design_Difficulty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(design_Difficulty);
        }

        // GET: Design_Difficulty/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Design_Difficulty design_Difficulty = db.Design_Difficulty.Find(id);
            if (design_Difficulty == null)
            {
                return HttpNotFound();
            }
            return View(design_Difficulty);
        }

        // POST: Design_Difficulty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pk_id_dd,difficulty,design_price")] Design_Difficulty design_Difficulty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(design_Difficulty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(design_Difficulty);
        }

        // GET: Design_Difficulty/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Design_Difficulty design_Difficulty = db.Design_Difficulty.Find(id);
            if (design_Difficulty == null)
            {
                return HttpNotFound();
            }
            return View(design_Difficulty);
        }

        // POST: Design_Difficulty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Design_Difficulty design_Difficulty = db.Design_Difficulty.Find(id);
            db.Design_Difficulty.Remove(design_Difficulty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
